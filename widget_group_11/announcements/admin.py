from django.contrib import admin
from .models import Announcement, Reaction

# Register your models here.

admin.site.register(Announcement)
admin.site.register(Reaction)