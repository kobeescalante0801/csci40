from django.db import models
from homepage.models import WidgetUser

# Create your models here.


class Announcement(models.Model):

    announcement_title = models.CharField(max_length=50)
    announcement_body = models.CharField(max_length=500)
    pub_date = models.DateField(auto_now_add=True)
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE, default='', related_name='author')

    def __str__(self):
        return (self.announcement_title)


class Reaction(models.Model):
    reaction_choices = [
        ("Like", "Like"),
        ("Love", "Love"),
        ("Angry", "Angry")
    ]
    announcement = models.ForeignKey(
        Announcement, on_delete=models.CASCADE, related_name='announcements')
    reaction_name = models.CharField(
        max_length=10, choices=reaction_choices, default="Like")
    tally = models.IntegerField(default=0)

    def __str__(self):
        return (self.reaction_name)
