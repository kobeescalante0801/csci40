from django.shortcuts import redirect, render
from django.http import HttpResponse, Http404
from django.views import View

from .forms import AnnouncementForm

from .models import Announcement, Reaction

# Create your views here.


class HomePageView(View):
    def get(self, request):
        announcement_list = Announcement.objects.order_by("-id")
        return render(request, "announcements/index.html", {"announcement_list": announcement_list})

def add_announcement(request):
    if request.method == "POST":
        announcement_form = AnnouncementForm(request.POST)
        if announcement_form.is_valid():
            announcement_form = announcement_form.save()
            return redirect("announcements:add_announcement")
    else:
        announcement_form = AnnouncementForm()
    return render(request, "announcements/forms.html", {"announcement_form": announcement_form})


def detail(request, announcement_id):
    try: 
        announcement = Announcement.objects.get(pk = announcement_id)
        reaction_list = Reaction.objects.filter(announcement_id=announcement.id)
    except Announcement.DoesNotExist:
        raise Http404("Announcement does not exist.")
    return render(request, "announcements/details.html", {"announcement": announcement, "reaction_list": reaction_list})
    

