from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='index'),
    path("<int:announcement_id>/details/", views.detail, name="detail"),
    path("add/", views.add_announcement, name="add_announcement"),
]

app_name = "widget_group_11"