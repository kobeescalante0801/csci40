from django.db import models
from django.urls import reverse 

# Create your models here.

# Add a model called Course with the following fields
class Course(models.Model):
    #course_code a string of alphanumeric characters with a maximum length of ten (10)
    course_code = models.CharField(max_length=10)
    #course_title - a string of alphanumeric characters for a course’s name
    course_title = models.CharField(max_length=100)
    #section - a string of alphabet characters with maximum length of of three (3)
    section = models.CharField(max_length=3)

    def __str__(self):
        return f"{self.course_code} {self.course_title} {self.section}"

class Assignment(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=250)
    max_points = models.IntegerField()
    # Represent the relationship that an Assignment is associated with a particular Course
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    # add a field called passing_score to the Assignment
    @property
    def passing_score(self):
        return int(0.60*self.max_points)

    def __str__(self):
        return f'Assignment Name: {self.name} Max Points: {self.max_points} Passing: {self.passing_score}'

    def get_absolute_url(self):
        # return f'/assignments/assignments_detail/{self.id}/'
        return f'/assignments/assignment/{self.id}/detail/'


