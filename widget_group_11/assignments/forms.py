from django.forms import ModelForm
from .models import Assignment, Course 

class AssignmentForm(ModelForm):
    class Meta:
        model = Assignment
        fields = ['name', 'description', 'max_points', 'course']

