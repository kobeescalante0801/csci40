from django.contrib import admin

# Register your models here.
from .models import Assignment, Course

class CourseAdmin(admin.ModelAdmin):
    model = Course

class AssignmentAdmin(admin.ModelAdmin):
    model = Assignment

admin.site.register(Assignment, AssignmentAdmin)
admin.site.register(Course, CourseAdmin)