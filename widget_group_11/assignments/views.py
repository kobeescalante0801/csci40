from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.template import Context
from .models import Assignment, Course
from .forms import AssignmentForm
# Create your views here.


def assignments_list(request):
    courses = Course.objects.all().order_by('course_code')
    context = dict()
    link_json = dict()

    for course in courses:
        assignments = Assignment.objects.all().filter(course=course)
        context[course.__str__()] = list()
        
        for assignment in assignments:
            context[course.__str__()].append(assignment.name)
            #context[course.__str__()+"_link"].append(assignment.id)
            link_json[assignment.name] = assignment.id

            
        
    context = {'courses': context, 'url': link_json }
        

    """
    assignments = Assignment.objects.all()
    text_format = "ASSIGNMENTS: <br /><br />"
    # Display all available Assignments and their respective Courses in the format
    for assignments_detail in assignments:
        text_format += f'''
            Assignment Name: {assignments_detail.name} <br />
            Description: {assignments_detail.description} <br />
            Perfect Score: {assignments_detail.max_points} <br />
            Passing Score: {assignments_detail.passing_score} <br />
            Course/Section: {assignments_detail.course}
        '''
        text_format += "<br /> <br />"
    return HttpResponse(text_format)

    """
    return render(request, 'assignments/assignment_list.html', context)
    
def assignments_detail(request, pk):
    assignments_detail = get_object_or_404(Assignment, pk=pk)
    print(f"{assignments_detail}")
    print(f"{assignments_detail.name}")

    text_format = f"""
        Assignment Name: {assignments_detail.name} <br />
        Description: {assignments_detail.description} <br />
        Perfect Score: {assignments_detail.max_points} <br />
        Passing Score: {assignments_detail.passing_score} <br />
        Course/Section: {assignments_detail.course}
    """
    context = {'assignment_detail': text_format}
    # return HttpResponse(text_format)
    return render(request, 'assignments/assignment_detail.html', context)

def assignments_add(request):
    # return HttpResponse('You are creating a new assignment!')
    form = None
    if request.method == "POST":
        form = AssignmentForm(request.POST)

        if form.is_valid():
            assignment = form.save(commit=False) # returns an Assignment Model
            assignment.save()
            return redirect('/')
    else:
        form = AssignmentForm()

    context = {'form': form}
    return render(request, "assignments/assignment_add.html", context)
