from django.urls import path
from .views import assignments_list, assignments_detail, assignments_add

urlpatterns = [
    path('', assignments_list, name='assignments_list'),
    path('assignment/<int:pk>/detail/', assignments_detail, name='assignment_detail'),
    path('add/', assignments_add, name='assignments_add'),

]

app_name = "widget_group_11"