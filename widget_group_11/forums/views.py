from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from django.views import View
from django.views.generic import RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from .forms import AddForm

from .models import Post, Reply

# Create your views here.
class HomePageView(ListView):
    model = Post
    template_name='forums/index.html'
    ordering =['-pub_date']

class PostDetailView(DetailView):
    model = Post
    template_name = 'forums/post-detail.html'
    context_object_name = 'post'

class AddPostView(FormView):
    template_name = 'forums/add-post.html'
    form_class = AddForm
    success_url = 'redirect/'

    def form_valid(self,form):
        form.save()
        return super().form_valid(form)

class Redirection(RedirectView):

    pattern_name = 'forums:index'
