from django.db import models
from datetime import datetime
from django.urls import reverse
from homepage.models import WidgetUser

# Create your models here.

class Post(models.Model):
    post_title = models.CharField(max_length=50)
    post_body = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published', default=datetime.now)
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE, default='')

    def __str__(self):
        return self.post_title

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    def get_absolute_url(self):
        return reverse('postdetailview', args=[str(self.id)])

    def save(self, *args, **kwargs):
        if not self.pub_date:
            self.pub_date = datetime.now
        return super().save(*args, **kwargs)

class Reply(models.Model):
    reply_body = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published', default=datetime.now)
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE, default='')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, default='')

    class Meta:
        verbose_name_plural = "Replies"

    def where_posted(self):
        return self.post
