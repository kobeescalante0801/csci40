from django.contrib import admin

from .models import *
# Register your models here.

class PostAdmin(admin.ModelAdmin):
    model = Post

    search_fields = ('post_title', 'pub_date',)
    list_display = ('post_title', 'pub_date',)
    list_filter = ('post_title', 'post_body',)

class ReplyAdmin(admin.ModelAdmin):
    model = Reply

    search_fields = ('reply_body',)
    list_display = ('reply_body',)
    list_filter = ('reply_body', 'pub_date', 'author',)

admin.site.register(Post, PostAdmin)
admin.site.register(Reply, ReplyAdmin)
