from django.urls import path
from .views import HomePageView, PostDetailView, AddPostView, Redirection

urlpatterns = [
    path('', HomePageView.as_view(), name='index'),
    path('posts/<int:pk>/details/', PostDetailView.as_view(), name='post_detail'),
    path('posts/add/', AddPostView.as_view(), name='add_post'),
    path('posts/add/redirect/', Redirection.as_view(), name='redirect'),
]

app_name = "widget_group_11"
