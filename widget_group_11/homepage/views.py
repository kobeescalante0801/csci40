from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView

from .models import WidgetUser
from .forms import UserForm


class UserListView(ListView):
    model = WidgetUser


class UserDetailView(DetailView):
    model = WidgetUser


class UserCreateView(CreateView):
    model = WidgetUser
    template_name = 'homepage/widgetuser_create.html'
    fields = '__all__'

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['first_name'].widget.attrs.update({'class': 'form-control'})
        form.fields['middle_name'].widget.attrs.update({'class': 'form-control'})
        form.fields['last_name'].widget.attrs.update({'class': 'form-control'})
        form.fields['id_num'].widget.attrs.update({'class': 'form-control'})
        form.fields['email'].widget.attrs.update({'class': 'form-control'})
        form.fields['profile_image'].widget.attrs.update({'class': 'form-control'})
        form.fields['department'].widget.attrs.update({'class': 'form-control'})
        return form
