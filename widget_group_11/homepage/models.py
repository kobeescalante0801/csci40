from django.db import models
from django.urls import reverse


class WidgetUser(models.Model):
    first_name = models.CharField(max_length=20)
    middle_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    id_num = models.CharField(max_length=7)
    email = models.EmailField(max_length=50)
    profile_image = models.ImageField(null=True, blank=True, upload_to="images/profile/")
    department = models.ForeignKey(
        'Department',
        on_delete=models.CASCADE,
        related_name='department'
    )

    def __str__(self):
        return '{}, {} {}'.format(self.last_name, self.first_name, self.middle_name)

    def get_absolute_url(self):
        return reverse('homepage:user-list')


class Department(models.Model):
    dept_name = models.CharField(max_length=100)
    home_unit = models.CharField(max_length=100)

    def __str__(self):
        return '{}, {}'.format(self.dept_name, self.home_unit)
