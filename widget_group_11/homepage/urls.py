from django.urls import path

from .views import UserListView, UserDetailView, UserCreateView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('homepage/', UserListView.as_view(), name='user-list'),
    path('users/<int:pk>/details', UserDetailView.as_view(), name='user-detail'),
    path('users/add/', UserCreateView.as_view(), name='user-create')
]+ static (settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

app_name = "widget_group_11"


