from django import forms


class UserForm(forms.Form):
    first_name = forms.CharField(max_length=20)
    middle_name = forms.CharField(max_length=20)
    last_name = forms.CharField(max_length=20)
    id_num = forms.CharField(max_length=7)
    email = forms.EmailField(max_length=50)
