from django.contrib import admin
from .models import WidgetUser, Department

admin.site.register(WidgetUser)
admin.site.register(Department)
